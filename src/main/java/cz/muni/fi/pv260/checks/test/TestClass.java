package cz.muni.fi.pv260.checks.test;

public class TestClass {

    int a = 1;
    int b = 2;
    int c = 3;
    int d = 4;
    int e = 5;

    // Variables = 3
    // CC = 1
    // Depth level = 0
    // LOC = 5
    static {
        int i = 1;
        int j = 2;
        int k = 3;
    }

    // Variables = 4
    // CC = 2
    // Depth level = 1
    // LOC = 6
    {
        int i = 1;
        int j = 2;
        int k = 3;
        int l = 4;
        while (l < 5) {
            l = 5;
        }
    }

    // Variables = 5
    // CC = 1
    // Depth level = 0
    // LOC = 8
    public void testOne() {
        int i = 1;
        int j = 2;
        i = 2;
        int k = 3;
        int l = 4;
        int m = 5;
    }

    // Variables = 6
    // CC = 7
    // Depth level = 4
    // LOC = 26
    public void testTwo() {
        int x = 0;
        int y = 0;
        int z = 0;
        int w = 0;
        try {
            int i = 0;

            while (i > -100) {
                i--;

                if (true) {
                    continue;
                }
            }

          } catch (IllegalArgumentException ex) {
              int a = 0;

              while (a < 100) {
                  a++;
                  if (true) {
                      if (false) {
                          continue;
                      }

                      continue;
                  }
              }
          }
    }

    // Variables = 1
    // CC = 6
    // Depth level = 3
    // LOC = 9
    public void testThree() {
        int i = 1;
        while (true) {
            if (true) {
                while (true || false && true) {
                    continue;
                }
            }
        }
    }

    // Variables = 5
    // CC = 7
    // Depth level = 4
    // LOC = 27
    public void testFour() {
        try {
            int i = 0;

            while (i > -100) {
                i--;

                if (true) {
                    int c = 0;
                    continue;
                }
            }

          } catch (IllegalArgumentException ex) {
              int a = 0;

              a = 0;
              a = 0;
              while (a < 100) {
                  a++;
                  if (true) {
                      int d = 9;
                      if (false) {
                          continue;
                      }

                      continue;
                  }
              }
          }

          int x = 123;
    }
}