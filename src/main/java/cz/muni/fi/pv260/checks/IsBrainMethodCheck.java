package cz.muni.fi.pv260.checks;

import com.puppycrawl.tools.checkstyle.api.AbstractCheck;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.FileContents;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;

public class IsBrainMethodCheck extends AbstractCheck {

    // CLASS ATTRIBUTES

    // Constants
    private static final int DEFAULT_MAX_VAR_COUNT = 6;
    private static final int DEFAULT_MAX_DEPTH_LEVEL = 4;
    private static final int DEFAULT_MAX_COMPLEXITY = 8;
    private static final int DEFAULT_MAX_LOC = 40;

    private static final int STARTING_VAR_COUNT = 0;
    private static final int STARTING_DEPTH_LEVEL = 0;
    private static final int STARTING_COMPLEXITY = 1;
    private static final int STARTING_LOC = 0;

    // State flag - remember if we are in a method or not
    private boolean inMethod;

    // Variable count-related state
    private int maxVariableCount;
    private int variableCount;

    // Nesting-related state
    private int maxDepthLevel;
    private int depthLevel;
    private boolean overMaxDepthLevel;

    // Complexity-related state
    private int maxComplexity;
    private int complexity;

    // LOC-related state
    private int maxLOC;
    private int linesOfCode;

    // CLASS CONSTRUCTORS

    public IsBrainMethodCheck() {
        inMethod = false;

        maxVariableCount = DEFAULT_MAX_VAR_COUNT;
        variableCount = STARTING_VAR_COUNT;

        maxDepthLevel = DEFAULT_MAX_DEPTH_LEVEL;
        depthLevel = STARTING_DEPTH_LEVEL;
        overMaxDepthLevel = false;

        maxComplexity = DEFAULT_MAX_COMPLEXITY;
        complexity = STARTING_COMPLEXITY;

        maxLOC = DEFAULT_MAX_LOC;
        linesOfCode = STARTING_LOC;
    }

    // SETTERS

    public void setMaxComplexity(int complexity) {
        maxComplexity = complexity;
    }

    public void setMaxDepthLevel(int level) {
        maxDepthLevel = level;
    }

    public void setMaxVariableCount(int count) {
        maxVariableCount = count;
    }

    public void setMaxLOC(int loc) {
        maxLOC = loc;
    }

    // TOKEN DEFINITIONS

    @Override
    public int[] getDefaultTokens() {
        return new int[]{
            // Methods
            TokenTypes.CTOR_DEF,
            TokenTypes.METHOD_DEF,
            TokenTypes.INSTANCE_INIT,
            TokenTypes.STATIC_INIT,
            // Variables
            TokenTypes.VARIABLE_DEF,
            // Depth-level related
            TokenTypes.LITERAL_WHILE,
            TokenTypes.LITERAL_DO,
            TokenTypes.LITERAL_FOR,
            TokenTypes.LITERAL_IF,
            TokenTypes.LITERAL_CASE,
            TokenTypes.LITERAL_TRY,
            // Additional complexity related
            TokenTypes.QUESTION,
            TokenTypes.LAND,
            TokenTypes.LOR
        };
    }

    @Override
    public int[] getRequiredTokens() {
        return new int[]{};
    }

    @Override
    public int[] getAcceptableTokens() {
        return new int[]{};
    }

    // MAIN CRAWL HANDLING

    @Override
    public void visitToken(DetailAST ast) {
        switch (ast.getType()) {
            case TokenTypes.CTOR_DEF:
            case TokenTypes.METHOD_DEF:
            case TokenTypes.INSTANCE_INIT:
            case TokenTypes.STATIC_INIT:
                // We are inside a method
                inMethod = true;
                linesOfCode = countLOC(ast);
                break;
            default:
                if (inMethod) {
                    visitInMethod(ast);
                }
                break;
        }
    }

    @Override
    public void leaveToken(DetailAST ast) {
        switch (ast.getType()) {
            case TokenTypes.CTOR_DEF:
            case TokenTypes.METHOD_DEF:
            case TokenTypes.INSTANCE_INIT:
            case TokenTypes.STATIC_INIT:
                logIfNecessary(ast);
                resetState();
                break;
            default:
                if (inMethod) {
                    leaveInMethod(ast);
                }
                break;
        }
    }

    // SPECIFIC HOOKS

    private void visitInMethod(DetailAST ast) {
        switch (ast.getType()) {
            case TokenTypes.VARIABLE_DEF:
                addNewVariable();
                break;
            case TokenTypes.LITERAL_WHILE:
            case TokenTypes.LITERAL_DO:
            case TokenTypes.LITERAL_FOR:
            case TokenTypes.LITERAL_IF:
            case TokenTypes.LITERAL_CASE:
            case TokenTypes.LITERAL_TRY:
                addDepthLevel();
                addComplexity();
                break;
            default:
                addComplexity();
                break;
        }
    }

    private void leaveInMethod(DetailAST ast) {
        switch (ast.getType()) {
            case TokenTypes.LITERAL_WHILE:
            case TokenTypes.LITERAL_DO:
            case TokenTypes.LITERAL_FOR:
            case TokenTypes.LITERAL_IF:
            case TokenTypes.LITERAL_CASE:
            case TokenTypes.LITERAL_TRY:
                removeDepthLevel();
                break;
            default:
                break;
        }
    }

    // HELPER METHODS

    private void addComplexity() {
        complexity++;
    }

    private void addDepthLevel() {
        if (depthLevel == maxDepthLevel) {
            overMaxDepthLevel = true;
        }
        depthLevel++;
    }

    private void addNewVariable() {
        variableCount++;
    }

    private int countLOC(DetailAST ast) {
        DetailAST openingBrace = ast.findFirstToken(TokenTypes.SLIST);
        if (openingBrace != null) {
            return getLinesBetweenBraces(openingBrace);
        }

        return 0;
    }

    private int getLinesBetweenBraces(DetailAST openingBrace) {
        DetailAST closingBrace = openingBrace.findFirstToken(TokenTypes.RCURLY);
        int length = closingBrace.getLineNo() - openingBrace.getLineNo() + 1;

        FileContents contents = getFileContents();
        int lastLine = closingBrace.getLineNo();

        // lastLine - 1 is actual last line index. Last line is line with curly brace,
        // which is always not empty. So, we make it lastLine - 2 to cover last line that
        // actually may be empty.
        for (int i = openingBrace.getLineNo() - 1; i <= lastLine - 2; i++) {
            if (contents.lineIsBlank(i) || contents.lineIsComment(i)) {
                length--;
            }
        }
        return length;
    }

    private boolean isBrainMethod() {
        boolean tooComplex = complexity > maxComplexity;
        boolean tooManyVariables = variableCount > maxVariableCount;
        boolean tooManyLOC = linesOfCode > maxLOC;

        return overMaxDepthLevel && tooManyVariables && tooComplex && tooManyLOC;
    }

    private void logIfNecessary(DetailAST ast) {
        if (isBrainMethod()) {
            log(ast, "Brain method.");
        }
    }

    private void removeDepthLevel() {
        depthLevel--;
    }

    private void resetState() {
        inMethod = false;
        overMaxDepthLevel = false;

        variableCount = STARTING_VAR_COUNT;
        depthLevel = STARTING_DEPTH_LEVEL;
        complexity = STARTING_COMPLEXITY;
        linesOfCode = STARTING_LOC;
    }
}