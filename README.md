# swqual_assign3

PV260 assignment 3

## Checkstyle Task

run `mvn clean install` to build the check

you can configure the check using the `src/main/resources/brainconfig.xml` file

there is a test class to run the check on, `cz.muni.fi.pv260.checks.test.TestClasss`
each method has information about the number of variables, CC, nesting depth and LOC
in a comment above it, so you may check different configuration of the IsBrainMethodCheck class

## Task 2

You can find the solution for Task 2 either in the assignment_03.markdown or assignment_03.pdf file.