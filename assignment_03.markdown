# Task 2

For each of the following problems, do the following:
1. Find at least 3 of the possible/probable root causes of the problem
2. Find at least 3 possible reasonable solutions or the next action steps.
3. For the possible solution you like the most, write at least 2 non-trivial problems you may encounter when implementing the solution.

Problems:

1. Customers often find out really serious security bugs affecting them, representing a threat to
   them.
   Some of the customers share that bug with public / actively write about it to other of your
   customers.
   1. 
	* Existence of these bugs in the first place (lack of testing?)
	* Non-disclosing existence of the bugs to the public.
	* Customers do not have/know of a proper and clear way to report issues to us directly.
   2. 
	* Bounty hunter program and clear issue reporting system.
	* Focus on fixing these security bugs instead of trying to implement new features.
	* Disclose existence of the bugs. (Isn't it illegal not to?)
   3. 
    * Solution 2: customer churn, inability to keep the customers engaged while fixing the bugs
        (nothing new is happening).
	* Solution 2: we may inadvertently have to rethink even the architecture, leading to major costs.

2. Your acceptance tests run 20 hours in CI.
   You cannot use CI for verification of emergency fixes as it takes too long to get relevant
   results from it.
   That means sometimes emergency fixes are released uncomfortably late (waiting for tests to be
   executed), sometimes not properly tested.
   1. 
	* Individual tests are badly designed and take too long to run.
	* All tests are always run, even when just a potentially isolatable component has been modified.
	* The environment in which the tests are run is not properly designed, it may have too few 
	    resources, or too much testing data to work with.
   2. 
	* Split the architecture from monolithic to isolated components that are easier/faster to test.
	* Verify that the testing environment is set up properly, has enough resources.
	* Verify that tests are well-designed, and there are no unnecessary tests (duplicating testing work).
   3. 
    * Solution 1: This is major overhaul, the whole architecture is to be rethought, leading
        to significant costs.
	* Solution 1: We now need to ensure that the components are properly communicating
	    with each other (they do not break their contracts).

3. Other teams (the same teams over and over again) often break your tests in CI in master and
   future-release branches.
   Most of those test failures catch real problems.
   You spend alot of time on failing test investigation and teams must often solve problems not
   long before the release.
   1. 
	* Lack of communication and documentation, leading to the other teams not properly understanding
	    their environment.
	* Those teams are inexperienced.
	* Lack of proper testing on the lower levels (unit).
   2. 
	* Assign senior team leaders to help the teams grow, cross-team code reviews.
	* Establish regular communication channels and mandatory documentation practices.
	* Implement TDD practices.
   3. 
    * Solution 2: Communication and documentation are a significant overhead, people may not be able
        to find time.
	* Solution 2: There needs to be a new Acceptance Criterion for documentation set up,
	    which needs to be checked for each implemented task.

4. Company is not using, but want to use Continuous Delivery and there have already been done
   analysis that the nature of their business allows Continuous Delivery in theory.

   1. ?
   2. 
	* Examine and identify company's current delivery processes, codify them (e.g. BPMN), identify
	    key requirements and current bottlenecks.
	* Research available continuous delivery tools to determine which will best suit the company's environment.
	* Design and implement continuous delivery pipelines and processes for individual work packages. 
   3. 
    * The company may need to invest into additional HW resources, and reengineer many of their processes.
	* The current status of work packages may have to be updated for continuous delivery to be effective
	    and even make sense (proper testing practices, architecture redesign, ...)

5. Company fixed a bug in production, then re-injected it to production again, then fixed it, then
   re-injected it again.
   This happened several times for several different bugs.
   1. 
	* An obscure bug solution, that is not documented well enough. (re-injected during refactorings)
	* Features with re-injected bugs are not tested sufficiently, regression tests are missing.
	* Tests exist, but aren't run everytime changes are made (CI).
   2. 
	* Check (and improve) code coverage.
	* Verify merging and code review strategies, ensure branches are merged properly, so as to
	    not reintroduce old code.
	* Check CI environment setup and verify tests are being properly executed.
   3. 
    * Solution 1: This may lead to a slowdown of development activities.
	* Solution 1: People will need to be trained in proper testing strategies, possibly including TDD.

6. QAs do not know what exactly to verify with bug fixes, they sometimes spend unnecessary too much
   time on the verification (they are retesting what automated tests already testedand/or figuring
   out for a long time what to test) and sometimes also do not catch some bugwhich they could and
   should caught and the bug gets it to production.
   1. 
	* There is lacking or no Issue Register (or Issue Tracker).
	* Lack of proper Testing Plan, and absence of testing scenarios.
	* Lack of information transfer between QA and Development.
   2. 
	* Set up issue tracking/register to keep track of various issues, responsibilities and their evolution.
	* Create explicit Acceptance Criteria and/or test scenarios for each feature.
	* Create QA review meetings for Dev and QA teams to exchange information and plan together.
   3. 
    * In general, there will need to happen major process reengineering and training among both Development and QA team.
	* Solution 1: People may be resistant to use issue tracking tools properly, creating even more chaos, rather than organization.

7. Your team is fixing bugs all the time and spends no/very little time for new features.
   Product Management wants to develop new features to improve the business.
   1. 
	* Features aren't tested well enough after being implemented
	* Lack of employees
	* Unrealistic Product Management's expectations
   2. 
	* Hire more people, possibly create a separate QA team
	* Improve development practices, maybe adopt TDD, so as to do both.
	* Manage up - negotiate with Product Managament and try to lower their expectation.
   3. 
    * Solution 1: Hiring takes lots of time, therefore it won't be a quick solution.
	* Solution 1: In a small company, additional salaries might not be a possibility.

8. One developer in your team likes to give very detailed code reviews to others with lots
   of comments and often insists on the comments being fixed even when other developers whose code is
   being reviewed, do not think those comments need to be fixed.
   1. 
	* He means it well as he's trying to improve code quality, even though he's nit-picking.
	* Reviewees are trying to protect their egos.
	* Team does not have code guides.
   2. 
	* Talk to him about the depth he should get into in his code reviews. There might be some double
		standards too, as he's having others adhere to higher standards than he adheres to himself.
	* Talk to the rest of developers. Reviewer might be right after all.
	* Write said guides about code smells and style.
   3. 
    * Solution 3: Coming up with a code guide and writing/choosing static analysis checker (and formatter)
        will take some time.
	* Solution 3: Having others adhere to new rules won't be easy. Legacy code might get incosistent
		and even harder to debug afterwards.

9. You are working in a big company with hundreds of developers.
   Mainline branch is very often broken, often needs to be locked to fix the failing tests and that
   blocks integration of new changes.
   1. 
	* New features aren't tested well enough before being deployed.
	* There is someone who develops in master.
	* CI isn't in use.
   2. 
	* Grant access rights such that commits need to be reviewed (by chosen people) before getting
		merged to master.
	* Find who breaks it and educate them on best practices of GIT usage.
	* Implement CI.
   3. 
    * Solution 3: CI won't fix these bugs, it will just make them easier to detect.
	* Solution 3: Running all the tests can take a lot of time - only some of the tests will be run.
		It is easy to depend on CI to catch all the bugs - those that won't be caught will therefore
		be harder to spot.

10. You are at the top development leadership in a big company which develops new and new versions
   of the cloud-based product and you make new release of the product every week.
   Your product is business critical for your customers and there are often injections with the new
   releases, which break basic functionality for some of your customers and they are often angry.
   The code coverage is around 40 percent.
   1. 
    * After getting traction, said company can not afford losing its momentum.
    * To release more often, company had to deploy more features (even untested ones).
    * Trying to appeal to the majority of customers (those that don't get their functionality
        broken), minority is not getting so much attention.
   2. 
 	* Write more tests in order to increase code coverage at the cost of releasing the
	    product less often.
 	* Consider adopting TDD.
 	* Hire new testers, create a QA team and issue register.
   3. 
    * Solution 1: Some customers might switch to a competition that will offer bleeding edge
        technology.
    * Solution 1: This will disrupt workflow of employees who are not used to thorough testing.

11. You are a QA lead and your employees often tell you it’s hard to cooperate with
   Technical Support to them as the Technical Support has often poor product knowledge.
   Also the percentage of issues ending up closed as invalid suggests your QAs might be true.
   1. 
    * New TS employees aren't trained properly.
    * TS employees don't believe in the product - therefore have no interest to learn about it.
    * Product is poorly documented.
   2. 
    * Spend more time training new TS employees.
    * Have TS use the product for some time, so as to have better knowledge of the product than
        just from documentation and marketing materials.
 	* TS team should be trained continually and keep an up-to-date knowledge base in order to be
 	    able to reflect on new changes.
   3. 
    * Solution 3: Training costs time, and will have to be scheduled carefully so as to not disrupt
        TS activities.
 	* Solution 3: Creating a knowledge base is, again, time-consuming, and there is always a risk of
 	    outdated information being present, which may result in TS workers making mistakes more frequently.

12. You are quite a new developer in a team assigned to do a Code Review for your colleague.
   It’s your first code review and he just added you to the code review and did not talk to you
   about that.
   You are not sure how to do the code review.
   1. 
    * They expect it to be taught at school.
    * They want me to show initiative and learn it on my own.
    * They believe that Code Reviews are common enough that they honestly don't expect anyone not to
        have any experience with those.
   2. 
    * If I'm a junior developer, I'll be honest and ask my supervisor.
 	* I'll self study and learn it on my own.
 	* I'll talk to the colleague and ask him to take me through the process and show me how it's
 	    supposed to be done.
   3. 
    * Solution 2: I will have to do it in my own time, unless my employer is benevolent.
    * Solution 2: If I do it while at work, my productivity will suffer a hit. Additionally, the first
        results of my code reviews may be subpar and not fulfill their purpose.

